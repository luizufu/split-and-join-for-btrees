all: manuscript

.PHONY: manuscript clean

manuscript:
	lualatex --shell-escape -output-directory=build manuscript.tex
	bibtex template
	lualatex --shell-escape -output-directory=build manuscript.tex
	lualatex --shell-escape -output-directory=build manuscript.tex

clean:
	rm -f build/*
